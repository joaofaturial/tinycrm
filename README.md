# Tiny CRM
## Description
Tiny CRM is a simple demo project that allows you to manage clients using a REST API.
## Configuration
This application can be configured by editing the properties file at `src/main/resources/application.properties`
## Running the application
### Locally
```shell
mvn spring-boot:run
```
### Docker
First build the image:
```shell
docker build . -t tinycrm
```
```shell
docker run tinycrm
```
## Tests
### Run tests
```shell
mvn test
```
### Nomenclature
``
<methodNameAndScenario>_<expected result>
``
For example:
``
findAddressesByCustomerIdForCustomerWithoutAddresses_returnsEmptyList
``
