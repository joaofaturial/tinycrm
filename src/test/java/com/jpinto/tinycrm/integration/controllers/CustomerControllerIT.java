package com.jpinto.tinycrm.integration.controllers;

import com.jpinto.tinycrm.integration.BaseIT;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@AutoConfigureMockMvc
public class CustomerControllerIT extends BaseIT {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void postCustomer_CheckCustomerWasCreated() throws Exception {

		this.mockMvc.perform(
			MockMvcRequestBuilders.post("/customers").contentType("application/json").content(
				"{ \"firstName\": \"Joao\", \"lastName\": \"Pinto\" }"
			)
		).andExpect(MockMvcResultMatchers.status().isCreated());
	}
}
