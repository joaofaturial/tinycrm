package com.jpinto.tinycrm.integration.services;

import com.jpinto.tinycrm.dtos.AddAddressToCustomerDTO;
import com.jpinto.tinycrm.dtos.CreateCustomerDTO;
import com.jpinto.tinycrm.entities.Customer;
import com.jpinto.tinycrm.entities.enums.AddressType;
import com.jpinto.tinycrm.integration.BaseIT;
import com.jpinto.tinycrm.services.CustomerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class CustomerServiceIT extends BaseIT {

	@Autowired
	private CustomerService customerService;

	@Test
	public void addValidAddressToExistingCustomer_addressInsertedOnDatabase(){

		CreateCustomerDTO createCustomerDTO = new CreateCustomerDTO();
		createCustomerDTO.setFirstName("Joao");
		createCustomerDTO.setLastName("Pinto");
		Customer createdCustomer = this.customerService.createCustomer(createCustomerDTO);

		AddAddressToCustomerDTO addAddress = new AddAddressToCustomerDTO();
		addAddress.setAddressDetail("Test street");
		addAddress.setCountry("Portugal");
		addAddress.setAddressType(AddressType.BILLING);

		var addressList = this.customerService.getCustomerAddressList(createdCustomer.getId());
		Assertions.assertEquals(0,addressList.size());

		this.customerService.addAddressToCustomer(
			addAddress,createdCustomer.getId()
		);

		addressList = this.customerService.getCustomerAddressList(createdCustomer.getId());
		Assertions.assertEquals(1,addressList.size());
		Assertions.assertEquals("Test street",addressList.get(0).getAddressDetail());
	}
}
