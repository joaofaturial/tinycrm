package com.jpinto.tinycrm.integration;

import com.jpinto.tinycrm.TinycrmApplication;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(
	classes = {TinycrmApplication.class},
	webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BaseIT {
}
