package com.jpinto.tinycrm.integration.repositories;

import com.jpinto.tinycrm.entities.Customer;
import com.jpinto.tinycrm.integration.BaseIT;
import com.jpinto.tinycrm.repositories.CustomerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.UUID;

public class CustomerRepositoryIT extends BaseIT {

	@Autowired
	private CustomerRepository customerRepository;

	@Test
	public void findCustomerByIdWithExistingCustomer_returnsExpectedCustomer(){
		UUID existingId = UUID.randomUUID();
		Customer c = saveCustomerWithId(existingId);

		var retrievedCustomerOptional = this.customerRepository.findById(existingId);

		Assertions.assertFalse(retrievedCustomerOptional.isEmpty());
		Assertions.assertEquals( retrievedCustomerOptional.get().getId(),existingId );
	}


	private Customer saveCustomerWithId(UUID id){
		Customer c = new Customer();
		c.setId(id);
		c.setFirstName("Demo");
		c.setLastName("Customer");
		c.setValidFrom(new Date());
		c.setIsDeleted(false);
		return this.customerRepository.save(c);
	}
}
