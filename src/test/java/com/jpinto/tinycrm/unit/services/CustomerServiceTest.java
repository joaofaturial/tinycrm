package com.jpinto.tinycrm.unit.services;

import com.jpinto.tinycrm.dtos.AddAddressToCustomerDTO;
import com.jpinto.tinycrm.entities.Address;
import com.jpinto.tinycrm.entities.Customer;
import com.jpinto.tinycrm.entities.enums.AddressType;
import com.jpinto.tinycrm.exceptions.EntityNotFoundException;
import com.jpinto.tinycrm.repositories.AddressRepository;
import com.jpinto.tinycrm.repositories.CustomerRepository;
import com.jpinto.tinycrm.services.CustomerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class CustomerServiceTest {

	private static final UUID notFoundId = UUID.randomUUID();
	private static final UUID foundId = UUID.randomUUID();
	private static final UUID customerIdWithoutAddresses = UUID.randomUUID();

	private CustomerRepository customerRepository;
	private AddressRepository addressRepository;
	private CustomerService customerService;

	public CustomerServiceTest(){

		mockCustomerRepository();
		mockAddressRepository();
		this.customerService = new CustomerService(customerRepository,addressRepository);
	}

	@Test
	public void findCustomerByIdForNonExistingId_invokesRepositoryFindByIdAndThrowsEntityNotFoundException(){

		Assertions.assertThrows(
			EntityNotFoundException.class,
			()->{
				Customer customer = this.customerService.findCustomerById(notFoundId);
			}
		);
		Mockito.verify(this.customerRepository).findById( Mockito.eq(notFoundId) );
	}


	@Test
	public void findAddressesByCustomerIdForCustomerWithoutAddresses_returnsEmptyList(){

		var addressList = this.customerService.getCustomerAddressList(customerIdWithoutAddresses);
		Mockito.when(addressRepository.findAddressesByCustomer(Mockito.argThat(c->c.getId().equals(customerIdWithoutAddresses)))).thenReturn(
			List.of()
		);
		Assertions.assertEquals(0,addressList.size());
	}

	@Test
	public void addValidAddressToCustomer_addressReturnedInCustomerAddressesList(){

		AddAddressToCustomerDTO addAddressToCustomerDTO = new AddAddressToCustomerDTO();
		addAddressToCustomerDTO.setAddressType(AddressType.BILLING);
		addAddressToCustomerDTO.setCountry("Portugal");
		addAddressToCustomerDTO.setAddressDetail("Rua de teste");
		addAddressToCustomerDTO.setZipCode("1600-121");

		this.customerService.addAddressToCustomer(
			addAddressToCustomerDTO,foundId
		);
		Mockito.when(addressRepository.findAddressesByCustomer(Mockito.argThat(c->c.getId().equals(foundId)))).thenReturn(
			List.of( new Address() )
		);

		var addressList = this.customerService.getCustomerAddressList(foundId);
		Assertions.assertEquals(1,addressList.size());
		Mockito.verify(this.addressRepository).save(Mockito.any());
	}


	private void mockCustomerRepository(){
		this.customerRepository = Mockito.mock(CustomerRepository.class);
		Mockito.when( customerRepository.findById( Mockito.eq(notFoundId) ) ).thenReturn( Optional.empty() );

		var foundCustomer = new Customer();
		foundCustomer.setId(foundId);
		Mockito.when( customerRepository.findById( Mockito.eq(foundId) ) ).thenReturn( Optional.of(foundCustomer) );

		var customerWithoutAddresses = new Customer();
		customerWithoutAddresses.setId(customerIdWithoutAddresses);
		Mockito.when( customerRepository.findById( Mockito.eq(customerIdWithoutAddresses) ) ).thenReturn( Optional.of(customerWithoutAddresses) );
	}

	private void mockAddressRepository(){
		this.addressRepository = Mockito.mock(AddressRepository.class);
	}
}
