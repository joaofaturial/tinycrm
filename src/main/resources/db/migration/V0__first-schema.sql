CREATE TABLE customer (
    id UUID NOT NULL,
    is_deleted BOOLEAN NOT NULL,
    valid_from DATETIME NOT NULL,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(20) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE address (
  id UUID NOT NULL,
  is_deleted BOOLEAN NOT NULL,
  created_at DATETIME NOT NULL,
  address_type VARCHAR(256) NOT NULL,
  address_detail VARCHAR(1024) NOT NULL,
  country VARCHAR(256) NOT NULL,
  state VARCHAR(256),
  zip_code VARCHAR(256),
  street_number VARCHAR(50),
  customer_id UUID,
  PRIMARY KEY (id),
  FOREIGN KEY (customer_id) REFERENCES customer(id)
);

