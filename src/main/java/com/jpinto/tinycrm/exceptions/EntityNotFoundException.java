package com.jpinto.tinycrm.exceptions;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class EntityNotFoundException extends RuntimeException{

	private String entityName;
	private String entityId;

	@Override
	public String getMessage(){
		return String.format("%s with id: %s not found",entityName,entityId);
	}
}
