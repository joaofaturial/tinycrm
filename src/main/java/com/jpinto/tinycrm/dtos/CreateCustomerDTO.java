package com.jpinto.tinycrm.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CreateCustomerDTO {

	@NotNull
	@Length(min=2,max=200)
	private String firstName;
	@NotNull
	@Length(min=2,max=200)
	private String lastName;
}
