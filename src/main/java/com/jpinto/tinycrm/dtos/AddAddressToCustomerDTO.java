package com.jpinto.tinycrm.dtos;

import com.jpinto.tinycrm.entities.enums.AddressType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AddAddressToCustomerDTO {

	@NotNull
	private AddressType addressType;
	@NotNull
	private String addressDetail;
	private String country;
	private String state;
	private String zipCode;
	private String streetNumber;

}
