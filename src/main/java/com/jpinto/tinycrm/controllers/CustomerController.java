package com.jpinto.tinycrm.controllers;

import com.jpinto.tinycrm.dtos.AddAddressToCustomerDTO;
import com.jpinto.tinycrm.dtos.CreateCustomerDTO;
import com.jpinto.tinycrm.entities.Address;
import com.jpinto.tinycrm.entities.Customer;
import com.jpinto.tinycrm.services.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
public class CustomerController{

	private CustomerService customerService;

	public CustomerController(
		CustomerService customerService
	){
		this.customerService = customerService;
	}

	@GetMapping("/customers/{id}")
	public Customer getCustomerByID(@PathVariable("id") UUID id){
		return this.customerService.findCustomerById(id);
	}

	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping("/customers")
	public Customer postCustomer(@RequestBody @Valid CreateCustomerDTO createCustomerDTO) {
		return this.customerService.createCustomer(createCustomerDTO);
	}

	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping("/customers/{customerId}/addresses")
	public Address postAddress(
		@RequestBody @Valid AddAddressToCustomerDTO addAddressToCustomerDTO,
		@PathVariable("customerId") UUID customerId
		) {
		return this.customerService.addAddressToCustomer(addAddressToCustomerDTO,customerId);
	}

	@GetMapping("/customers/{customerId}/addresses")
	public List<Address> getAddresses(@PathVariable("customerId") UUID customerId
	) {
		return this.customerService.getCustomerAddressList(customerId);
	}
}
