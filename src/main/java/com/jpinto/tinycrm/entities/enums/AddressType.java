package com.jpinto.tinycrm.entities.enums;

public enum AddressType {
	SHIPPING,
	BILLING
}
