package com.jpinto.tinycrm.entities;

import com.jpinto.tinycrm.entities.enums.AddressType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
public class Address {

	@Id
	private UUID id;
	private Boolean isDeleted;
	private Date createdAt;

	@Enumerated(EnumType.STRING)
	private AddressType addressType;

	private String addressDetail;
	private String country;
	private String state;
	private String zipCode;
	private String streetNumber;

	@ManyToOne
	private Customer customer;
}
