package com.jpinto.tinycrm.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
public class Customer {

	@Id
	private UUID id;
	private Boolean isDeleted;
	private Date validFrom;

	private String firstName;
	private String lastName;
}
