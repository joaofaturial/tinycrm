package com.jpinto.tinycrm.services;

import com.jpinto.tinycrm.dtos.AddAddressToCustomerDTO;
import com.jpinto.tinycrm.dtos.CreateCustomerDTO;
import com.jpinto.tinycrm.entities.Address;
import com.jpinto.tinycrm.entities.Customer;
import com.jpinto.tinycrm.exceptions.EntityNotFoundException;
import com.jpinto.tinycrm.repositories.AddressRepository;
import com.jpinto.tinycrm.repositories.CustomerRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class CustomerService{

	private CustomerRepository customerRepository;
	private AddressRepository addressRepository;

	public CustomerService(CustomerRepository customerRepository,AddressRepository addressRepository){
		this.customerRepository = customerRepository;
		this.addressRepository = addressRepository;
	}

	public Customer findCustomerById(UUID id){
		return this.customerRepository.findById(id).orElseThrow( ()->new EntityNotFoundException("Customer",id.toString()));
	}

	public Customer createCustomer(CreateCustomerDTO createCustomerDTO){

		var newCustomer = new Customer();
		newCustomer.setId(UUID.randomUUID());
		newCustomer.setIsDeleted(false);
		newCustomer.setValidFrom(new Date());
		newCustomer.setFirstName(createCustomerDTO.getFirstName());
		newCustomer.setLastName(createCustomerDTO.getLastName());
		return this.customerRepository.save(newCustomer);
	}

	public List<Address> getCustomerAddressList(UUID customerId){
		var customer = this.findCustomerById(customerId);
		return this.addressRepository.findAddressesByCustomer(customer);
	}

	@Transactional
	public Address addAddressToCustomer(AddAddressToCustomerDTO addAddressToCustomerDTO, UUID customerId){

		var customer = this.findCustomerById(customerId);
		Address newAddress = mapDtoToAddress(addAddressToCustomerDTO,customer);
		return this.addressRepository.save(newAddress);
	}

	private Address mapDtoToAddress(AddAddressToCustomerDTO dto,Customer customer){
		Address address = new Address();

		address.setId(UUID.randomUUID());
		address.setIsDeleted(false);
		address.setCreatedAt(new Date());
		address.setCustomer(customer);
		address.setAddressType(dto.getAddressType());
		address.setAddressDetail(dto.getAddressDetail());
		address.setStreetNumber(dto.getStreetNumber());
		address.setCountry(dto.getCountry());
		address.setState(dto.getState());
		return address;
	}
}
