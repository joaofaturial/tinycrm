package com.jpinto.tinycrm.repositories;

import com.jpinto.tinycrm.entities.Address;
import com.jpinto.tinycrm.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface AddressRepository extends JpaRepository<Address, UUID> {

	List<Address> findAddressesByCustomer(Customer customer);
}
