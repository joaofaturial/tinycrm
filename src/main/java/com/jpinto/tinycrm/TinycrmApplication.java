package com.jpinto.tinycrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@ComponentScan(basePackages = {
	"com.jpinto.tinycrm.services",
	"com.jpinto.tinycrm.controllers",
	"com.jpinto.tinycrm.advices"
})
public class TinycrmApplication {

	public static void main(String[] args) {
		SpringApplication.run(TinycrmApplication.class, args);
	}

}
