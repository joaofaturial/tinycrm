FROM ubuntu:20.04

WORKDIR /opt/tinycrm

ADD rs2ctrl/target/tinycrm-0.0.1-SNAPSHOT.jar .
RUN chmod +x tinycrm-0.0.1-SNAPSHOT.jar

RUN apt-get install openjdk-11-jre -y

EXPOSE 8080

ENTRYPOINT java -jar tinycrm-0.0.1-SNAPSHOT.jar
